var _ = require('underscore')
    , get = require('./lib/utils').get
    , baseapi = 'http://api.hotwire.com/v1';

function Hotwire(apikey) {
    this.apikey = apikey;
};

Hotwire.prototype.hotelDeals = function (params, callback) {
    get(baseapi, '/deal/hotel', _.extend(params, {apikey: this.apikey}), callback);
};
Hotwire.prototype.hotelSearch = function (params, callback) {
    get(baseapi, '/search/hotel', _.extend(params, {apikey: this.apikey}), callback);
};
Hotwire.prototype.rentalCarSearch = function (params, callback) {
    get(baseapi, '/search/car', _.extend(params, {apikey: this.apikey}), callback);
};
Hotwire.prototype.tripStarterHotelSearch = function (params, callback) {
    get(baseapi, '/tripstarter/hotel', _.extend(params, {apikey: this.apikey}), callback);
};
Hotwire.prototype.tripStarterAirSearch = function (params, callback) {
    get(baseapi, '/tripstarter/air', _.extend(params, {apikey: this.apikey}), callback);
};

module.exports = Hotwire;