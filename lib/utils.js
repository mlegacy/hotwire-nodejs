var request = require('request')
    _ = require('underscore');

var makeParams = function (params) {
    return _.map(_.pairs(params), function (p) { return p.join('='); }).join('&');
};

exports.get = function (api, endpoint, params, callback) {
    request(api + endpoint + '?' + makeParams(params), callback);
};