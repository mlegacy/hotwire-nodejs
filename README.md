# hotwire -- Simple API Wrapper for the Hotwire.com Developer API

[![NPM](https://nodei.co/npm/hotwire.png)](https://nodei.co/npm/hotwire/)

## Really easy to use
## API Documentation can be found at: http://developer.hotwire.com/docs/read/Home


```javascript
var Hotwire = require('hotwire');

var hotwire = new Hotwire(<API key>);

//API Endpoint Details:  http://developer.hotwire.com/docs/read/Hotel_Deals_API
hotwire.hotelDeals({format: 'json', dest: 'Chicago'}, function (err, response, body) {
    if (err) { console.log(err); }
    console.log(response.statusCode);
    console.log(body);
});


//API Endpoint Details:  http://developer.hotwire.com/docs/read/Hotel_Shopping_API
hotwire.hotelSearch({format: 'json', dest: 'Memphis', rooms: 1, adults: 2, children: 0, startdate: '05/15/2014', enddate: '05/23/2014'}, function (err, response, body) {
    if (err) { console.log(err); }
    console.log(response.statusCode);
    console.log(body);
});


//API Endpoint Details:  http://developer.hotwire.com/docs/read/Rental_Car_Shopping_API
hotwire.rentalCarSearch({format: 'json', dest: 'LAX', startdate: '05/20/2014', enddate: '05/23/2014', pickuptime: '10:00', dropofftime: '13:30'}, function (err, response, body) {
    if (err) { console.log(err); }
    console.log(response.statusCode);
    console.log(body);
});


//TripStarter API Endpoint Details:  http://developer.hotwire.com/docs/read/TripStarter_APIs
hotwire.tripStarterHotelSearch({format: 'json', price: '*~75', sort: 'date', limit: 1}, function (err, response, body) {
    if (err) { console.log(err); }
    console.log(response.statusCode);
    console.log(body);
});

hotwire.tripStarterAirSearch({format: 'json', price: '*~75', sort: 'date', limit: 1}, function (err, response, body) {
    if (err) { console.log(err); }
    console.log(response.statusCode);
    console.log(body);
});
```